//
//  SessionLoginModelResponse.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 04/08/2022.
//

import Foundation

struct SessionLoginModelResponse: Decodable {
    var jwt: String
}
