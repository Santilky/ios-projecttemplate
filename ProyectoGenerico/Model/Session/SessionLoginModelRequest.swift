//
//  SessionLoginModelRequest.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 04/08/2022.
//

import Foundation

struct SessionLoginModelRequest: Encodable {
    var email: String
    var password: String
}
