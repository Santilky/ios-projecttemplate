//
//  SwapiPeopleResponse.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation

struct SwapiPeopleResponse: Decodable {
    var results: [SwapiPeople]
}
struct SwapiPeople: Decodable {
    var name: String
    var hair_color: String
    var eye_color: String
    var height: String
}
