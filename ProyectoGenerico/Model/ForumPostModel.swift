//
//  ForumPostModel.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 04/08/2022.
//

import Foundation

struct ForumPostModel: Decodable {
    let id: Int
    let title: String
    let content: String
    
    init(dbModel: ForumPostDBModel) {
        id = 0
        title = dbModel.title ?? "No Title"
        content = dbModel.content ?? "No content"
    }
}
