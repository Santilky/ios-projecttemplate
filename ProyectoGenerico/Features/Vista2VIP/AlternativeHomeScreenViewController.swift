//
//  AlternativeHomeScreenViewController.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 28/07/2022.
//

import Foundation
import UIKit

class AlternativeHomeScreenViewController: UIViewController {
    var cells: [ReusableCell] = []
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.registerCells()
        return tableView
    }()
    var interactor: AlternativeHomeScreenInteractor
    init() {
        interactor = .init()
        super.init(nibName: nil, bundle: nil)
        let presenter = AlternativeHomeScreenPresenter()
        interactor.presenter = presenter
        interactor.presenter?.viewController = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        setupView()
        setupTableView()
        interactor.setupInitialState()
    }
    private func setupView() {
        view.backgroundColor = Colors.primarySurface
        tableView.reloadData()
    }
    private func setupTableView() {
        view.insert(view: tableView, top: 0, bottom: 0, left: 0, right: 0)
    }
    func reloadData() {
        tableView.reloadData()
    }
    func clearCell() {
        cells = []
    }
    func addCell(cell: ReusableCell) {
        cells.append(cell)
    }
}
extension AlternativeHomeScreenViewController: UITableViewDataSource, UITableViewDelegate  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = cells[indexPath.row]
        return model.reuseCell(tableView: tableView, indexPath: indexPath)
    }
}
