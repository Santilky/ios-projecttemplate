//
//  AlternativeHomeScreenInteractor.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 28/07/2022.
//

import Foundation

class AlternativeHomeScreenInteractor {
    var presenter: AlternativeHomeScreenPresenter?
    var worker: AlternativeHomeScreenWorker?
    init(worker: AlternativeHomeScreenWorker = .init()) {
        self.worker = worker
    }
    
    func setupInitialState() {
        presenter?.generateTitle(text: "Hola Mundo")
//        worker?.getPosts(onSuccess: { [weak self] posts in
//            for post in posts {
//                self?.presenter?.generateTitleDescription(title: post.title, text: post.content)
//            }
//            self?.presenter?.reloadView()
//        }, onError: { error in
//            print(error)
//        })
        let posts = worker?.getPostsFromDB()
        for post in posts! {
            presenter?.generateTitleDescription(title: post.title, text: post.content)
        }
        self.presenter?.reloadView()
        
    }
}
