//
//  AlternativeHomeScreenWorker.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 28/07/2022.
//

import Foundation

class AlternativeHomeScreenWorker {
    var api: APIProtocol
    init(api: APIProtocol = API.shared) {
        self.api = api
    }
    func getAllCharacters(onSuccess: @escaping (SwapiPeopleResponse) -> Void, onError: @escaping (SwapiError) -> Void) {
        api.getSWAPIPeople(onSuccess: onSuccess, onError: onError)
    }
    func getPosts(onSuccess: @escaping ([ForumPostModel]) -> Void, onError: @escaping (SwapiError) -> Void) {
        api.getPosts(onSuccess: onSuccess, onError: onError)
    }
    func getPostsFromDB() -> [ForumPostModel] {
        return BusinessStorage.shared.fetchContent()
    }
}
