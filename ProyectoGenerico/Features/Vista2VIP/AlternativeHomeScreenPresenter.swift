//
//  AlternativeHomeScreenPresenter.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 28/07/2022.
//

import Foundation

class AlternativeHomeScreenPresenter {
    weak var viewController: AlternativeHomeScreenViewController?
    
    func generateTitle(text: String) {
        let model = LabelTableViewModel(title: text)
        viewController?.addCell(cell: model)
    }
    func generateTitleDescription(title: String, text: String) {
        let model = TitleDescriptionTableViewModel(titulo: title, descripcion: text)
        viewController?.addCell(cell: model)
    }
    func clearCells() {
        viewController?.clearCell()
    }
    func reloadView() {
        viewController?.reloadData()
    }
}
