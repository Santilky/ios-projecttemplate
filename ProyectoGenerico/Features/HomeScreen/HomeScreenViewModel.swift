//
//  HomeScreenViewModel.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation

class HomeScreenViewModel {
    var cells: [ReusableCell] = []
    var onAPIFinish: (() -> Void)?
    private var title: String = "Hola Mundo"
    private var title2: String = "Bienvenidos a MVVM"
    var api: APIProtocol
    init(api: APIProtocol = API.shared) {
        self.api = api
    }
    func onLoadView() {
        fetchSwapiCharacters()
        let model1 = LabelTableViewModel(title: title)
        let model2 = LabelTableViewModel(title: title2)
        let model3 = TitleDescriptionTableViewModel(titulo: "Hoy subió el dolar!", descripcion: "Algunos estan contentos, otros no, el dolar llego a un pico de 340 en CCL, y cerró en 338")
        
        cells.append(model1)
        cells.append(model3)
        cells.append(model2)
        print("Empieza")
        NonHelper.hacerAlgo()
        print("termina")
    }
    private func fetchSwapiCharacters() {
        api.getSWAPIPeople(onSuccess: { [weak self] response in
            self?.processSwapiCharacters(characters: response.results)
        }, onError: { [weak self] error in
            print(error)
        })
    }
    private func processSwapiCharacters(characters: [SwapiPeople]) {
        for character in characters {
            cells.append(TitleDescriptionTableViewModel(titulo: character.name, descripcion: "Altura: \(character.height), color de ojos: \(character.eye_color)"))
        }
        onAPIFinish?()
    }
}
