//
//  HomeScreenViewController.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation
import UIKit

class HomeScreenViewController: UIViewController {
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.registerCells()
        return tableView
    }()
    var viewModel: HomeScreenViewModel
    init(viewModel: HomeScreenViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        viewModel.onLoadView()
        viewModel.onAPIFinish = { [weak self] in
            self?.reload()
        }
        setupView()
        setupTableView()
    }
    func setupView() {
        view.backgroundColor = Colors.primarySurface
        tableView.reloadData()
    }
    func reload() {
        tableView.reloadData()
    }
    func setupTableView() {
        view.insert(view: tableView, top: 0, bottom: 0, left: 0, right: 0)
    }
}
extension HomeScreenViewController: UITableViewDataSource, UITableViewDelegate  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = viewModel.cells[indexPath.row]
        return model.reuseCell(tableView: tableView, indexPath: indexPath)
    }
}
