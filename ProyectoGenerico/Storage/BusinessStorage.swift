//
//  BusinessStorage.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 11/08/2022.
//

import Foundation
import CoreData

class BusinessStorage {
    static var shared = BusinessStorage()
    var context: NSManagedObjectContext?

    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "BusinessStorage")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        self.context = container.viewContext
        return container
    }()
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        print(String(format: "$ %@", "54"))
        print("asd")
    }
    func createContent() {
        
        let model = ForumPostDBModel(context: context!)
        model.title = "Hola Mundo"
        model.content = "Esto es CoreData!"
        do {
            try context?.save()
        } catch {
            print("No se pudo guardar el contexto")
        }
    }

    func fetchContent() -> [ForumPostModel] {
        let request = ForumPostDBModel.fetchRequest()
        do {
            let result = try context?.fetch(request)
            if let result = result {
                var posts: [ForumPostModel] = []
                for item in result {
                    if let item = item as? ForumPostDBModel {
                        posts.append(ForumPostModel(dbModel: item))
                    }
                }
                return posts
            } else {
                print("no hay contenido")
            }
        } catch {
            print("Hubo un error: \(error.localizedDescription)")
        }
        return []
    }
}
