//
//  SessionDefaults.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 02/08/2022.
//

import Foundation

class SessionDefaults {
    static var shared: SessionDefaults = .init()

    func saveJWTToken(token: String) {
        UserDefaults.standard.set(token, forKey: "JWTToken")
    }
    func getJWTToken() -> String? {
        UserDefaults.standard.string(forKey: "JWTToken")
    }
}
