//
//  CellProtocols.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation
import UIKit

protocol ReusableCell {
    func reuseCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
}
