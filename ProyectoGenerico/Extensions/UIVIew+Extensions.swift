//
//  UIVIew+Extensions.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation
import UIKit

extension UIView {
    func insert(view: UIView, top: CGFloat? = nil, bottom: CGFloat? = nil, left: CGFloat? = nil, right: CGFloat? = nil, width: CGFloat? = nil, height: CGFloat? = nil) {

        var constraints: [NSLayoutConstraint] = []
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        if let top = top {
            constraints.append(view.topAnchor.constraint(equalTo: self.topAnchor, constant: top))
        }
        if let bottom = bottom {
            constraints.append(view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -bottom))
        }
        if let left = left {
            constraints.append(view.leftAnchor.constraint(equalTo: self.leftAnchor, constant: left))
        }
        if let right = right {
            constraints.append(view.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -right))
        }
        if let width = width {
            constraints.append(view.widthAnchor.constraint(equalToConstant: width))
        }
        if let height = height {
            constraints.append(view.heightAnchor.constraint(equalToConstant: height))
        }
        NSLayoutConstraint.activate(constraints)
    }
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        layer.shadowRadius = 1
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
