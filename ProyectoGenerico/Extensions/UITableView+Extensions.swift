//
//  UITableView+Extensions.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation
import UIKit

extension UITableView {
    func registerCells() {
        register(LabelTableViewCell.self, forCellReuseIdentifier: LabelTableViewCell.identifier)
        register(TitleDescriptionTableViewCell.self, forCellReuseIdentifier: TitleDescriptionTableViewCell.identifier)
        register(ButtonTableViewCell.self, forCellReuseIdentifier: ButtonTableViewCell.identifier)
        register(InputTextButtonTableViewCell.self, forCellReuseIdentifier: InputTextButtonTableViewCell.identifier)
    }
}
