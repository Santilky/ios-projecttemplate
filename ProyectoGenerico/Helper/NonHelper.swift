//
//  NonHelper.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 26/07/2022.
//

import Foundation
import UIKit

class NonHelper {
    
    
    static func hacerAlgo() {
        DispatchQueue.main.async {
            
        }
        DispatchQueue.global(qos: .default).async {
            let url = URL(string: "https://www.trecebits.com/wp-content/uploads/2020/02/meme-kid.jpg")
            do {
                let _ = try Data(contentsOf: url!)
            } catch {
                
            }
        }
    }
}
