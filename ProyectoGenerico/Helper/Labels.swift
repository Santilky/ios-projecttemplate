//
//  Labels.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation
import UIKit

final class LabelsHelper {
    static func createLabel(_ text: String? = nil, attributedString: NSMutableAttributedString? = nil, color: UIColor? = Colors.primaryText, font: UIFont? = Fonts.body1, alignment:  NSTextAlignment? = .center) -> UILabel {
        let label = UILabel()
        if let text = text {
            label.text = text
        }
        if let attributedString = attributedString {
            label.attributedText = attributedString
        }
        label.textColor = color
        label.font = font
        label.numberOfLines = 0
        if let alignment = alignment {
            label.textAlignment = alignment
        }
        return label
    }
}
