//
//  Fonts.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation
import UIKit

enum Fonts {
    static var title1: UIFont = .systemFont(ofSize: 24)
    static var title2: UIFont = .systemFont(ofSize: 22)
    static var title3: UIFont = .systemFont(ofSize: 20)
    static var body1: UIFont = .systemFont(ofSize: 20)
    static var body2: UIFont = .systemFont(ofSize: 18)
    static var body3: UIFont = .systemFont(ofSize: 16)
}
