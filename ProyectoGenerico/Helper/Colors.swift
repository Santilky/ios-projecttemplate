//
//  Colors.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation
import UIKit

enum Colors {
    static var primaryText: UIColor = UIColor(red: 10/255, green: 10/255, blue: 10/255, alpha: 1)
    static var primarySurface: UIColor = UIColor(red: 222/255, green: 222/255, blue: 222/255, alpha: 1)
    
    static var redTransparent: UIColor = .init(red: 222/255, green: 20/255, blue: 20/255, alpha: 0.5)
}
