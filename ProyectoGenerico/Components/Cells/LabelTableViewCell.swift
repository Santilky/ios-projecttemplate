//
//  LabelTableViewCell.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation
import UIKit


class LabelTableViewCell: UITableViewCell {
    static var identifier = "LabelTableViewCell"
    private(set) lazy var title: UILabel = {
        let label = LabelsHelper.createLabel(font: Fonts.title1)
        return label
    }()
    func configure(model: LabelTableViewModel) {
        title.text = model.title
        selectionStyle = .none
        contentView.insert(view: title, top: 24, bottom: 26, left: 16, right: 16)
    }
}
struct LabelTableViewModel: ReusableCell {
    let title: String
    func reuseCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier, for: indexPath) as? LabelTableViewCell else {
            return .init()
        }
        cell.configure(model: self)
        return cell
    }
}
