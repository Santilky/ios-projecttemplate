//
//  InputTextButtonTableViewCell.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 26/07/2022.
//

import Foundation
import UIKit

class InputTextButtonTableViewCell: UITableViewCell, UITextFieldDelegate {
    static var identifier = "InputTextButtonTableViewCell"
    var onAction: ((String) -> Void)?
    private lazy var textField: UITextField = {
        let textfield = UITextField()
        textfield.textColor = Colors.primaryText
        textfield.textAlignment = .left
        textfield.delegate = self
        return textfield
    }()

    func setup(data: InputTextButtonTableViewModel) {
        let view = UIView()
        self.onAction = data.onAction
        view.layer.cornerRadius = 8
        view.backgroundColor = Colors.primarySurface
        textField.placeholder = data.placeHolder
        view.insert(view: textField, top: 8, bottom: 8, left: 8, right: 8, height: 32)
        contentView.insert(view: view, top: 4, bottom: 4, left: 24, right: 24)
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        onAction?(textField.text ?? "default")
    }
}
//
struct InputTextButtonTableViewModel: ReusableCell {
    let onAction: (String) -> Void
    let placeHolder: String
    let title: String
    func reuseCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: InputTextButtonTableViewCell.identifier, for: indexPath) as? InputTextButtonTableViewCell else {
            return .init()
        }
        cell.setup(data: self)
        cell.selectionStyle = .none
        return cell
    }
}
