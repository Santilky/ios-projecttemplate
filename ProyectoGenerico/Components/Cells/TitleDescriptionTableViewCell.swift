//
//  TitleDescriptionTableViewCell.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation
import UIKit

class TitleDescriptionTableViewCell: UITableViewCell {
    static var identifier = "TitleDescriptionTableViewCell"
    private(set) lazy var tituloLabel: UILabel = {
        let label = LabelsHelper.createLabel(font: Fonts.title2, alignment: .left)
        return label
    }()
    private(set) lazy var descripcionLabel: UILabel = {
        let label = LabelsHelper.createLabel(font: Fonts.body2, alignment: .left)
        return label
    }()
    private(set) lazy var separator: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor(red: 50/255, green: 50/255, blue: 50/255, alpha: 1)
        return view
    }()
    func configure(model: TitleDescriptionTableViewModel) {
        tituloLabel.text = model.titulo
        descripcionLabel.text = model.descripcion
        setupConstraints()
    }
    func setupConstraints() {
        let view = UIView(frame: .zero)
        view.backgroundColor = Colors.redTransparent
        view.layer.cornerRadius = 12
        view.layer.borderWidth = 1
        view.layer.borderColor = CGColor(red: 50/255, green: 50/255, blue: 50/255, alpha: 1)
        contentView.insert(view: view, top: 24, bottom: 24, left: 16, right: 16)
        view.insert(view: tituloLabel, top: 24, left: 16, right: 16)
        view.insert(view: descripcionLabel, bottom: 24, left: 16, right: 16)
        tituloLabel.bottomAnchor.constraint(equalTo: descripcionLabel.topAnchor, constant: -24).isActive = true
    }
}

struct TitleDescriptionTableViewModel: ReusableCell {
    var titulo: String
    var descripcion: String

    func reuseCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: TitleDescriptionTableViewCell.identifier, for: indexPath) as? TitleDescriptionTableViewCell else {
            return .init()
        }
        cell.configure(model: self)
        cell.selectionStyle = .none
        return cell
    }
    
    
}
