//
//  ButtonTableviewCell.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 04/08/2022.
//

import Foundation
import UIKit

class ButtonTableViewCell: UITableViewCell {
    static var identifier = "ButtonTableViewCell"
    private lazy var button: UIButton = {
        let button: UIButton = .init()
        button.layer.cornerRadius = 16
        button.addTarget(self, action: #selector(onClick), for: .touchDown)
        return button
    }()
    private var action: (() -> Void)?
    
    func configure(model: ButtonTableViewModel) {
        contentView.insert(view: button, top: 16, bottom: 16, left: 24, right: 24, height: 48)
        button.setTitle(model.title, for: .normal)
        button.setTitleColor(model.titleColor, for: .normal)
        button.backgroundColor = model.backgroundColor
        button.dropShadow()
        action = model.action
    }

    @objc func onClick() {
        action?()
    }
    
}
struct ButtonTableViewModel: ReusableCell {
    let title: String
    let backgroundColor: UIColor?
    let titleColor: UIColor?
    let action: (() -> Void)?
    func reuseCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: ButtonTableViewCell.identifier, for: indexPath) as? ButtonTableViewCell else {
            return .init()
        }
        cell.configure(model: self)
        cell.selectionStyle = .none
        return cell
    }
}
