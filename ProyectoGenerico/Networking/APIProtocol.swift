//
//  APIProtocol.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 26/07/2022.
//

import Foundation

protocol APIProtocol {
    func getSWAPIPeople(onSuccess: @escaping (SwapiPeopleResponse) -> Void, onError: @escaping (SwapiError) -> Void)
    func loginToForumServer(model: SessionLoginModelRequest, onSuccess: @escaping (SessionLoginModelResponse) -> Void, onError: @escaping (SwapiError) -> Void)
    func getPosts(onSuccess: @escaping ([ForumPostModel]) -> Void, onError: @escaping (SwapiError) -> Void)
}
