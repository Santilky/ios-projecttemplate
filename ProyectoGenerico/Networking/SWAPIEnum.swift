//
//  SWAPIEnum.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 26/07/2022.
//

import Foundation
import Alamofire

enum SWAPIEnum {
    case people
    case specificPeople(id: String)
    case planet
    case specificPlanet(id: String)
    
    var method: String {
        switch self {
        case .people, .specificPeople, .planet, .specificPlanet:
            return "GET"
        }
    }
    var urlString: String {
        switch self {
        case .people:
            return "https://swapi.dev/api/people"
        case let .specificPeople(id):
            return "https://swapi.dev/api/people/\(id)"
        case .planet:
            return "https://swapi.dev/api/planet"
        case let .specificPlanet(id):
            return "https://swapi.dev/api/planet/\(id)"
        }
    }
}

enum ForumAPI {
    case login(request: SessionLoginModelRequest)
    case getPosts
    case respondPost
    case register
    
    static var baseUrl = "http://192.168.1.2:3000/api/"
    
    var apiURL: String {
        switch self {
        case .login:
            return ForumAPI.baseUrl + "user/session/login"
        case .getPosts:
            return ForumAPI.baseUrl + "post"
        default:
            return ""
        }
        var contentType: String {
            "application/json"
        }
    }
    var method: String {
        switch self {
        case .login, .respondPost, .register:
            return "POST"
        case .getPosts:
            return "GET"
        }
    }
    var body: Data? {
        switch self {
        case let .login(data):
            do {
                let encoder = JSONEncoder()
               return try encoder.encode(data)
            } catch {
                return nil
            }
        default:
            return nil
        }
    }
    var needsToken: Bool {
        switch self {
        case .getPosts, .respondPost:
            return true
        default:
            return false
        }
    }
}
