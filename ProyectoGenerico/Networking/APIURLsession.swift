//
//  APIURLsession.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 26/07/2022.
//

import Foundation

enum APIKeys {
    static var apikey: String? {
        ProcessInfo.processInfo.environment["miapikey"]
    }
}
class APIURLSession: APIProtocol {
    
    var session = URLSession(configuration: .default)

    func getSWAPIPeople(onSuccess: @escaping (SwapiPeopleResponse) -> Void, onError: @escaping (SwapiError) -> Void) {
        let urlString = SWAPIEnum.people.urlString
        guard let url = URL(string: urlString) else {
            onError(.serverError)
            return
        }
            var request = URLRequest(url: url)
        request.httpMethod = SWAPIEnum.people.method
            self.fetchData(request: request, onSuccess: onSuccess, onError: onError)
    }
    func fetchData<T:Decodable>(request: URLRequest, onSuccess: @escaping (T) -> Void, onError: @escaping (SwapiError) -> Void) {
        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data, let response = response else {
                return
            }
            print(data)
            print(response)
            print(error)
            let decoder = JSONDecoder()
            do {
                let response = try decoder.decode(T.self, from: data)
                DispatchQueue.main.async {
                    onSuccess(response)
                }
            } catch {
                DispatchQueue.main.async {
                    onError(.decodingError)
                }
            }
        }
        task.resume()
    }
    func loginToForumServer(model: SessionLoginModelRequest, onSuccess: @escaping (SessionLoginModelResponse) -> Void, onError: @escaping (SwapiError) -> Void) {
        let urlString = "http://192.168.1.2:3000/api/user/session/login"
        guard let url = URL(string: urlString) else {
            onError(.serverError)
            return
        }
            var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(model)
            request.httpBody = data
            fetchData(request: request, onSuccess: onSuccess, onError: onError)
        } catch {
            onError(.decodingError)
        }
    }
    func getResponse<T:Decodable>(api: ForumAPI, onSuccess: @escaping (T) -> Void, onError: @escaping (SwapiError) -> Void) {
        if let request = self.generateRequest(apiType: .getPosts) {
            fetchData(request: request, onSuccess: onSuccess, onError: onError)
        } else {
            onError(.serverError)
        }
    }
    func getPosts(onSuccess: @escaping ([ForumPostModel]) -> Void, onError: @escaping (SwapiError) -> Void) {
        if let request = self.generateRequest(apiType: .getPosts) {
            fetchData(request: request, onSuccess: onSuccess, onError: onError)
        } else {
            onError(.serverError)
        }
    }
    func generateRequest(apiType: ForumAPI) -> URLRequest? {
        guard let url = URL(string: apiType.apiURL) else {return nil}
        var request = URLRequest(url: url)
        request.httpMethod = apiType.method
        
        if let data = apiType.body {
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = data
        }
        if let token = SessionDefaults.shared.getJWTToken(), apiType.needsToken {
            request.addValue("bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        return request
    }
}
