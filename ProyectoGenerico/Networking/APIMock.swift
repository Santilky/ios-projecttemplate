//
//  APIMock.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 26/07/2022.
//

import Foundation

class APIMock: APIProtocol {

    
    public var loginResponse: SessionLoginModelResponse? = .init(jwt: "1234.1234.1234")
    public var response: SwapiPeopleResponse? = .init(
        results:
            [
                .init(name: "Han Solo", hair_color: "Brown", eye_color: "Brown", height: "180")
            ]
    )
    
    public var error: SwapiError?
    
    
    func getSWAPIPeople(onSuccess: @escaping (SwapiPeopleResponse) -> Void, onError: @escaping (SwapiError) -> Void) {
        if let response = response {
            onSuccess(
                response
            )
            onSuccess(response)
        } else {
            if let error = error {
                onError(error)
            } else {
                onError(.decodingError)
            }
        }
    }
    func loginToForumServer(model: SessionLoginModelRequest, onSuccess: @escaping (SessionLoginModelResponse) -> Void, onError: @escaping (SwapiError) -> Void) {
        if let loginResponse = loginResponse {
            onSuccess(loginResponse)
        } else {
            onError(.serverError)
        }
    }
    func getPosts(onSuccess: @escaping ([ForumPostModel]) -> Void, onError: @escaping (SwapiError) -> Void) {
        
    }
    
}
