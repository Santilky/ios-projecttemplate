//
//  API.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 21/07/2022.
//

import Foundation
import Alamofire

class API: APIProtocol {

    
    // Singleton(no puro) + Strategy
    static var shared: APIProtocol = APIMock()

    func getSWAPIPeople(onSuccess: @escaping (SwapiPeopleResponse) -> Void, onError: @escaping (SwapiError) -> Void) {
        let request = AF.request("https://swapi.dev/api/people")
        request.responseData() { response in
            guard let data = response.data else {
                onError(.serverError)
                return
            }
            let decoder = JSONDecoder()
            do {
                let response = try decoder.decode(SwapiPeopleResponse.self, from: data)
                onSuccess(response)
            } catch {
                onError(.decodingError)
            }
        }
    }
    func loginToForumServer(model: SessionLoginModelRequest, onSuccess: @escaping (SessionLoginModelResponse) -> Void, onError: @escaping (SwapiError) -> Void) {
        
    }
    func getPosts(onSuccess: @escaping ([ForumPostModel]) -> Void, onError: @escaping (SwapiError) -> Void) {
        
    }
    
}

enum SwapiError {
    case serverError
    case decodingError
}
