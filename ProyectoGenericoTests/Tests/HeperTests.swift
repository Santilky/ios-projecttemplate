//
//  HeperTests.swift
//  ProyectoGenericoTests
//
//  Created by Santiago Linietsky on 28/07/2022.
//

import Foundation
import XCTest
@testable import ProyectoGenerico

class HelperTests: XCTestCase {
    var vm: HomeScreenViewModel!
    var api: APIMock!
    override func setUp() {
        api = APIMock()
        vm = HomeScreenViewModel(api: api)
    }
    func testHelper() {
        // Given
        let text = "Hola!"

        // When
        let label = LabelsHelper.createLabel(text)
        // Then
        
        XCTAssertEqual(label.text, text, "Texts should be the same")
    }
    func testViewModel() {
        // Given
        var onApiFinishedCalled = false
        vm.onAPIFinish = {
            onApiFinishedCalled = true
        }
        // When
        vm.onLoadView()
        
        //Then
        XCTAssert(onApiFinishedCalled, "Value should be truthy")
        XCTAssert(vm.cells.count == 5, "Should have 5 cells")
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.5) { [weak self] in
            XCTAssert(self?.vm.cells.count == 0)
        }
        vm.cells = []
    }
    func testViewModelOnError() {
        // Given
        api.response = nil
        var onApiFinishedCalled = false
        vm.onAPIFinish = {
            onApiFinishedCalled = true
        }
        // When
        vm.onLoadView()
        
        //Then
        XCTAssert(!onApiFinishedCalled, "Value should be falsy")
        XCTAssert(vm.cells.count == 3, "Should have 3 cells")
        
    }
}
