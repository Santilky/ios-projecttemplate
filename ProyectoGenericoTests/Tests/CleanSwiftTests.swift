//
//  CleanSwiftTests.swift
//  ProyectoGenericoTests
//
//  Created by Santiago Linietsky on 28/07/2022.
//

import Foundation
import XCTest
@testable import ProyectoGenerico

class AlternativeViewTests: XCTestCase {
    func testPresenterTitleView() {
        let presenter = AlternativeHomeScreenPresenter()
        let vc = AlternativeHomeVCMock()
        presenter.viewController = vc
        let text = "Hola Mundo!"
        presenter.generateTitle(text: text)
        var model: LabelTableViewModel?
        if let cellModel = vc.getFirstCell() {
            model = cellModel as? LabelTableViewModel
        }
        XCTAssert(vc.hasAddCellBeenCalled, "The method has been called")
        XCTAssertEqual(model?.title, text, "Texts should be equal")
    }
    
}
class AlternativeHomeVCMock: AlternativeHomeScreenViewController {
    var hasAddCellBeenCalled = false
    override func addCell(cell: ReusableCell) {
        hasAddCellBeenCalled = true
        cells.append(cell)
    }
    func getFirstCell() -> ReusableCell? {
        return cells.first
    }
}
class LoginViewPresenterSpy: LoginViewPresentationLogic {
    func presentSomething(response: LoginView.Something.Response) {
        
    }
}


