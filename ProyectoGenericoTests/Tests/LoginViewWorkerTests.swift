//
//  LoginViewWorkerTests.swift
//  ProyectoGenerico
//
//  Created by Santiago Linietsky on 28/07/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

@testable import ProyectoGenerico
import XCTest

class LoginViewWorkerTests: XCTestCase
{
  // MARK: Subject under test
  
  var sut: LoginViewWorker!
  
  // MARK: Test lifecycle
  
  override func setUp()
  {
    super.setUp()
    setupLoginViewWorker()
  }
  
  override func tearDown()
  {
    super.tearDown()
  }
  
  // MARK: Test setup
  
  func setupLoginViewWorker()
  {
    sut = LoginViewWorker()
  }
  
  // MARK: Test doubles
  
  // MARK: Tests
  
  func testSomething()
  {
    // Given
    
    // When
    
    // Then
  }
}
